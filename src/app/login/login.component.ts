import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http'
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
		private router: Router,
		private authService: AuthService,
	) { }

	formInput = new FormGroup({
		email: new FormControl('', [
			Validators.required,
			Validators.email
		]),
		password: new FormControl('', Validators.required)
	})

	urlApi = 'https://bem-ipb.herokuapp.com/user/'

  ngOnInit(): void {
  }

	onSubmit(): void { 
		const { email, password } = this.formInput.value

		this.authService.login({
			email,
			password
		}).subscribe((data:any) => {
			if(data) {
				this.router.navigate([''])
				localStorage.setItem('access_token',data.token);
			}
		})
	}

}

