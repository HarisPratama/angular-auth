import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router';

import { UserService } from '../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	users: any = null;

	loadingOnSubmit:boolean = false;

	form = new FormGroup({
		name: new FormControl('', Validators.required),
		email: new FormControl('', [
			Validators.required,
			Validators.email,
		]),
		password: new FormControl('', Validators.required),
	})

	urlApi = 'https://63478aa3db76843976ad4a8a.mockapi.io/api/vq/users'

  constructor(
		private userService: UserService,
		private router: Router
	) { }

  ngOnInit(): void {
		this.getData()
  }

	getData() {
		const getData = this.userService.getUsers()

		getData.subscribe(data => {
			this.users = data
		})
	}

	onSubmit(): void { 
		this.loadingOnSubmit = true;

		const createUser = this.userService.createUser(this.form.value)
	
		createUser.subscribe(data => {
			if (data) {
				this.getData()
				this.loadingOnSubmit = false;
			}
		})
	}

	logOut() {
		// localStorage.removeItem('access_token');
		localStorage.clear()
		this.router.navigate(['/login']);
	}

}
