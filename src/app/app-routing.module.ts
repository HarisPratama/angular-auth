import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DetailComponent } from './detail/detail.component';

import { PermissionGuard } from './helpers/permission.guard';


const routes: Routes = [
	{
		path: '', 
		component: HomeComponent, 
		canActivate: [PermissionGuard]
	},
	{
		path: 'login', 
		component: LoginComponent,
		canActivate: [PermissionGuard]
	},
	{
		path: 'register', 
		component: RegisterComponent,
		canActivate: [PermissionGuard]
	},
	{
		path: 'detail/:id',
		title: 'Detail user',
		component: DetailComponent,
	},
	{
		path: '**',
		component: PageNotFoundComponent
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
