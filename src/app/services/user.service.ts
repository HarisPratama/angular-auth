import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

	users:  any[] = [];
	urlApi = 'https://63478aa3db76843976ad4a8a.mockapi.io/api/vq/users'

  constructor(
		private http: HttpClient
	) { }

	getUsers() {
		return this.http.get(this.urlApi)
	}

	createUser(data: any) {
		return this.http.post(this.urlApi, data)
	}

}
