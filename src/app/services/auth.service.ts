import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
		private http: HttpClient,
	) { }

	urlApi = 'https://bem-ipb.herokuapp.com/user/'

	login(data:any) {
		return this.http.post(`${ this.urlApi }/login`, data)
	}
}
