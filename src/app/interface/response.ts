export interface ResponseLogin {
	message: string;
	role: string;
	status: number;
	token: string;
}