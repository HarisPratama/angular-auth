import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

	constructor(
		private router: Router,
	) {

	}	

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
			
		const getAccessToken = localStorage.getItem('access_token');

		const activePath = state.url;

		switch (activePath) {
			case "/register":
			case "/login":
				getAccessToken ? this.router.navigate(['']) : true;
				return true;
			default:
				getAccessToken ? true : this.router.navigate(['login']);

				return true;
		}

  }
  
}
